#include "endgame.h"
#include <QVBoxLayout>
EndGame::EndGame(QWidget *parent) :
    QWidget(parent){}
EndGame::EndGame(int x){
    resize(800,600);
    //设置again
    Again = new QPushButton("Again");
    Again->setFont(QFont("Algerian",50));
    Again->setStyleSheet("QPushButton{background: transparent; color:black; }"
                         "QPushButton:hover{color:yellow;}");

    //设置game over
    Lose = new QLabel(this);
    Lose->setText("Game Over");
    Lose->setFont(QFont("Algerian",50));
    Lose->setStyleSheet("QLabel{background: transparent; color:white; }"
                        );
    //展示分数
    Score = new QLabel(this);
    Score->setText(QString(SCORE).arg(x));
    Score->setFont(QFont("Algerian",50));
    Score->setStyleSheet("QLabel{background: transparent; color:black; }"
                         );

    //垂直布局
    QVBoxLayout *lay = new QVBoxLayout;
    lay->addWidget(Lose);
    lay->addWidget(Score);
    lay->addWidget(Again);

    setLayout(lay);

    setAutoFillBackground(true);
    //载入game over图片
    QPalette pal;
    QPixmap pixmap = QPixmap(":/new/prefix1/Soundimage/gameover.png").scaled(this->size());
    pal.setBrush(QPalette::Window, QBrush(pixmap));
    this->setPalette(pal);
    connect(Again,&QPushButton::clicked,this,&EndGame::AgainClicked);  
}

EndGame::~EndGame(){}
//点击函数
void EndGame::AgainClicked(){
    Classic *e = new Classic;
    e->show();
    this->close();
}
