#ifndef BOMB_H
#define BOMB_H
#include <QPixmap>
#include "pro.h"
class Bomb
{
public:
    pro PRO;
    Bomb();
    float x;
    float y;
    float speed;
    bool isReverse;
    bool isFree;
    bool isDestroyed;
    bool isMissed;
    QPixmap bomb;
    void updatePosition();
};

#endif // BOMB_H
