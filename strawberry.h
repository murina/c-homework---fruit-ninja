#ifndef STRAWBERRY_H
#define STRAWBERRY_H
#include <QPixmap>
#include "pro.h"
class Strawberry
{
public:
    pro PRO;
    Strawberry();
    float x;
    float y;
    float speed;
    bool isReverse;
    bool isFree;
    bool isDestroyed;
    bool isMissed;
    QPixmap strawberry;
    void updatePosition();
};

#endif // STRAWBERRY_H
