#include "choose.h"
#include <QPixmap>
#include <QGridLayout>
#include <QSoundEffect>
//选择关卡1还是关卡2窗口
Choose::Choose()
{
setAttribute(Qt::WA_DeleteOnClose,true);
resize(800,600);
setWindowTitle("Choose");

//设置关卡按钮
classic = new QPushButton("关卡1");
classic->setFont(QFont("Algerian",40));
classic->setStyleSheet("QPushButton{background: transparent; color:black; }"
                       "QPushButton:hover{color:red;}");
classic2 = new QPushButton("关卡2");
classic2->setFont(QFont("Algerian",40));
classic2->setStyleSheet("QPushButton{background: transparent; color:black; }"
                        "QPushButton:hover{color:red;}");
//设置quit按钮
Quit = new QPushButton("Quit");
Quit->setFont(QFont("Algerian",60));
Quit->setStyleSheet("QPushButton{background: transparent; color:white; }"
                    "QPushButton:hover{color:red;}");
//设置标签文字
label1 = new QLabel(this);
label1->setFont(QFont("Algerian",60));
label1->setStyleSheet("QLabel{background: transparent; color:white; }"                    );
label1->setText("Classic:");

//加入关卡1、2
Recored = new QPushButton("Recored");
Recored->setGeometry(20,20,450,100);
QHBoxLayout* lay1 = new QHBoxLayout;
lay1->addWidget(classic);
lay1->addWidget(classic2);

//网格布局
QGridLayout *mainlay = new QGridLayout;
mainlay->addWidget(label1,0,0);
mainlay->addLayout(lay1,1,0);
mainlay->addWidget(Quit,3,1);
setLayout(mainlay);

//设置封面图片
setAutoFillBackground(true);
QPalette pal;;
QPixmap pixmap = QPixmap(":/new/prefix1/Soundimage/cover.png");
pixmap=pixmap.scaled(800,600);
pal.setBrush(QPalette::Window, QBrush(pixmap));
this->setPalette(pal);

//建立联系
connect(classic,&QPushButton::clicked,this,&Choose::ClassicClicked);
connect(classic2,&QPushButton::clicked,this,&Choose::Classic2Clicked);
connect(Quit,&QPushButton::clicked,this,&Choose::QuitClicked);

//播放背景音乐
QSoundEffect *startsound=new QSoundEffect;
startsound->setSource(QUrl::fromLocalFile(":/new/prefix1/Soundimage/background.wav"));
startsound->play();
startsound->setLoopCount(QSoundEffect::Infinite);//设置为一直循环播放
}
//点击信号函数
void Choose::ClassicClicked(){
    Classic *e = new Classic;
    e->show();
    this->close();
}


void Choose::Classic2Clicked(){
    Classic2 *e = new Classic2;
    e->show();
    this->close();
}


void Choose::QuitClicked(){
    this->close();
}

Choose::~Choose(){
}
