#include "bomb.h"

Bomb::Bomb() {
    bomb.load(":/new/prefix1/Soundimage/bomb.png");
    x=rand()%2*(600-bomb.width());
    y = 0;
    isFree=true;
    isMissed =false;
    isDestroyed = false;
    speed = 0;
    isReverse=0;
}
void Bomb::updatePosition(){
    if(!isFree)
    {
        if(isReverse==0)
        {
            x+=1.1;
            y+=0.004*x-1.2;
        }
        else
        {
            x-=1.1;
            y+=0.004*(600-x)-1.2;
        }
    }
    if(y>600||x>800||x<0||y<0)
    {
        isFree = true;
        isMissed =true;
    }
}
