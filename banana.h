#ifndef BANANA_H
#define BANANA_H
#include <QPixmap>
#include "pro.h"
class Banana
{
public:
    pro PRO;
    Banana();
    float x;
    float y;
    float speed;
    bool isReverse;
    bool isFree;
    bool isDestroyed;
    bool isMissed;
    QPixmap banana;
    void updatePosition();
};

#endif // BANANA_H
