#ifndef CHOOSE_H
#define CHOOSE_H
#include <QDialog>
#include <QPushButton>
#include <QHBoxLayout>
#include <QSoundEffect>
#include "widget.h"
#include "classic.h"
#include "classic2.h"

class Choose: public QWidget
{

  Q_OBJECT
public:
    Choose();
    QSoundEffect *startsound;
    QPushButton* classic;
    QPushButton* classic2;
    QLabel *label1;
    QLabel *label2;
    QPushButton *Recored;
    QPushButton *Quit;
    ~Choose();
public slots:
    void ClassicClicked();
    void Classic2Clicked();
    void QuitClicked();
};

#endif // CHOOSE_H
