#ifndef PEACH_H
#define PEACH_H
#include <QPixmap>
#include "pro.h"
class Peach
{
public:
    pro PRO;
    Peach();
    float x;
    float y;
    float speed;
    bool isReverse;
    bool isFree;
    bool isDestroyed;
    bool isMissed;
    QPixmap peach;
    void updatePosition();
};

#endif // PEACH_H
