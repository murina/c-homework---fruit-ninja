#ifndef CLASSIC2_H
#define CLASSIC2_H
#include <QWidget>
#include <QTimer>
#include <knife.h>
#include <QPainter>
#include <QPaintEvent>
#include <QKeyEvent>
#include <QMouseEvent>
#include <banana.h>
#include <apple.h>
#include <bomb.h>
#include <watermelon.h>
#include <peach.h>
#include <strawberry.h>
#include <QLabel>
#include <endgame.h>
#include <QSoundEffect>
#define FDSOUND ":/new/prefix1/Soundimage/cut.wav"
#define SCORE "Score: %1"
#define LIFE  "Life:  %1"

class Classic2 : public QWidget
{
    Q_OBJECT
public:
    bool win;
    bool winPlayed;
    int Score;
    QSoundEffect* Player1;
    QLabel* score;
    QLabel* life;
    Knife myKnife;
    QTimer Timer;
    Banana banana[20];
    Watermelon watermelon[20];
    Apple apple[20];
    Peach peach[20];
    Strawberry strawberry[20];
    int fruitRecorded;
    Bomb bomb[8];
    void initial();
    void startGame();
    void endGame();
    void updatePositino();
    void paintEvent(QPaintEvent *E);
    void mouseMoveEvent(QMouseEvent *E);
    void FruitShow();
    void GameWin();
    void collisionDetetion();
    int getDistanceBAK(Banana B,Knife K);
    int getDistanceWAK(Watermelon W,Knife K);
    int getDistanceTAK(Bomb T,Knife K);
    int getDistanceAAK(Apple A,Knife K);
    int getDistancePAK(Peach P,Knife K);
    int getDistanceSAK(Strawberry S,Knife K);
    explicit Classic2(QWidget *parent = nullptr);
    ~Classic2();

signals:

};

#endif // CLASSIC2_H
