#include "rules.h"
#include <QVBoxLayout>
Ruleswidget::Ruleswidget(QWidget *parent) :
    QWidget(parent)
{
    setAutoFillBackground(true);
    //载入背景图片
    QPalette pal;
    QPixmap pixmap = QPixmap(":/new/prefix1/Soundimage/background.png").scaled(this->size());
    pal.setBrush(QPalette::Window, QBrush(pixmap));
    this->setPalette(pal);
    //阐述规则
    Label1 = new QLabel(this);
    Label1->setFont(QFont("Algerian",18));
    Label1->setStyleSheet("QLabel{background:transparent;color:white;}");

    Label2 = new QLabel(this);
    Label2->setFont(QFont("Algerian",18));
    Label2->setStyleSheet("QLabel{background:transparent;color:white;}");

    Label3 = new QLabel(this);
    Label3->setFont(QFont("Algerian",18));
    Label3->setStyleSheet("QLabel{background:transparent;color:white;}");

    Label4 = new QLabel(this);
    Label4->setFont(QFont("Algerian",18));
    Label4->setStyleSheet("QLabel{background:transparent;color:white;}");

    Label5 = new QLabel(this);
    Label5->setFont(QFont("Algerian",18));
    Label5->setStyleSheet("QLabel{background:transparent;color:white;}");

    Label6 = new QLabel(this);
    Label6->setFont(QFont("Algerian",18));
    Label6->setStyleSheet("QLabel{background:transparent;color:white;}");

    Label7 = new QLabel(this);
    Label7->setFont(QFont("Algerian",18));
    Label7->setStyleSheet("QLabel{background:transparent;color:white;}");

    Label8 = new QLabel(this);
    Label8->setFont(QFont("Algerian",18));
    Label8->setStyleSheet("QLabel{background:transparent;color:white;}");

    Label9 = new QLabel(this);
    Label9->setFont(QFont("Algerian",18));
    Label9->setStyleSheet("QLabel{background:transparent;color:white;}");

    Label10 = new QLabel(this);
    Label10->setFont(QFont("Algerian",18));
    Label10->setStyleSheet("QLabel{background:transparent;color:white;}");

    Label11 = new QLabel(this);
    Label11->setFont(QFont("Algerian",18));
    Label11->setStyleSheet("QLabel{background:transparent;color:white;}");

    Label1->setText("本游戏为《水果忍者》的模仿游戏");
    Label2->setText("在游戏中，玩家需通过拖拽鼠标移动刀来进行操作");
    Label3->setText("共有三条生命");
    Label4->setText("切到水果会增加分数，但遗漏水果或切到炸弹将会让生命值减少");
    Label5->setText("关卡分为一二两关 第二关较第一关难度略高");
    Label6->setText("玩得开心！");
    Label7->setText("     ");
    Label8->setText("");
    Label9->setText( "");
    Label10->setText("");
    Label11->setText("");
    //垂直布局
    QVBoxLayout *lay = new QVBoxLayout;
    lay->addWidget(Label1);
    lay->addWidget(Label2);
    lay->addWidget(Label3);
    lay->addWidget(Label4);
    lay->addWidget(Label5);
    lay->addWidget(Label6);
    lay->addWidget(Label7);
    lay->addWidget(Label8);
    lay->addWidget(Label9);
    lay->addWidget(Label10);
    lay->addWidget(Label11);

    setLayout(lay);
}
Ruleswidget::~Ruleswidget()
{
}
