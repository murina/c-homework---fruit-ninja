#include "widget.h"
#include "ui_widget.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPalette>
#include <QBrush>
#include <classic.h>
#include <classic2.h>
#include <rules.h>
#include <choose.h>

//封面窗口
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    resize(800,600);

    //开始游戏
    startGame = new QPushButton("Start Game");
    startGame->setFont(QFont("Algerian",40));
    startGame->setStyleSheet("QPushButton{background: transparent; color:black; }"
                             "QPushButton:hover{color:red;}");

    //游戏规则
    Rules = new QPushButton("Rules");
    Rules->setFont(QFont("Algerian",40));
    Rules->setStyleSheet("QPushButton{background: transparent; color:black; }"
                        "QPushButton:hover{color:red;}");

    //关闭游戏
    closeGame = new QPushButton("Close Game");
    closeGame->setFont(QFont("Algerian",40));
    closeGame->setStyleSheet("QPushButton{background: transparent; color:black; }"
                        "QPushButton:hover{color:red;}");

    //标题
    label = new QLabel(this);
    label->setText("FRUIT NINJA");
    label->setFont(QFont("Algerian",80));
    label->setStyleSheet("QLabel{background:transparent;color:white;}");

    //垂直布局
    QVBoxLayout* lay = new QVBoxLayout;
    lay->addWidget(label);
    lay->addWidget(startGame);
    lay->addWidget(Rules);
    lay->addWidget(closeGame);
    setLayout(lay);

    //载入封面图片
    setAutoFillBackground(true);
    QPalette pal;
    QPixmap pixmap = QPixmap(":/new/prefix1/Soundimage/cover.png").scaled(this->size());
    pal.setBrush(QPalette::Window, QBrush(pixmap));
    this->setPalette(pal);

    //建立联系
    connect(startGame,&QPushButton::clicked,this,&Widget::startClick);
    connect(closeGame,&QPushButton::clicked,this,&Widget::closeClick);
    connect(Rules,&QPushButton::clicked,this,&Widget::RulesClick);
}

//信号函数
void Widget::startClick(){
    Choose *enter=new Choose;
    enter->show();
    this->close();
}
void Widget::closeClick(){
    this->close();
}

void Widget::RulesClick(){
   Ruleswidget *rules=new Ruleswidget;
   rules->show();
}

Widget::~Widget()
{
    delete ui;
}

