#include "apple.h"

Apple::Apple() {
    apple.load(":/new/prefix1/Soundimage/apple.png");//载入图像
    x=rand()%2*(600-apple.width());//位置随机出现
    y = 0;
    isFree=true;
    isMissed =false;
    isDestroyed = false;
    speed = 0;//速度
    isReverse=0;
}
void Apple::updatePosition(){//位置更新
    if(!isFree)
    {
        if(isReverse==0)
        {
            x+=1.1;
            y+=0.004*x-1.2;
        }
        //使位置变化
        else
        {
            x-=1.1;
            y+=0.004*(600-x)-1.2;
        }
    }
    if(y>650||x>850||x<0||y<0)
    {
        isFree = true;
        isMissed =true;//水果从界面中消失
    }
}
