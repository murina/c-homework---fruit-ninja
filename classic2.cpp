#include "classic2.h"
#include <ctime>
#include <cmath>
#include <QMessageBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QString>
#include <iostream>
using namespace std;
Classic2::Classic2(QWidget *parent) : QWidget(parent)
{
    QWidget::setCursor(QCursor(Qt::BlankCursor));
    setAttribute(Qt::WA_DeleteOnClose,true);
    resize(800,600);
    Score = 0;
    setAutoFillBackground(true);
    QPalette pal;
    QPixmap pixmap = QPixmap(":/new/prefix1/Soundimage/background.png").scaled(this->size());
    pal.setBrush(QPalette::Window, QBrush(pixmap));
    this->setPalette(pal);
    life=new QLabel(this);
    score=new QLabel(this);
    life->setFont(QFont("Algerian",20));
    life->setStyleSheet("QLabel{background:transparent;color:white;}");
    score->setFont(QFont("Algerian",20));
    score->setStyleSheet("QLabel{background:transparent;color:white;}");
    //垂直布局
    QVBoxLayout *lay = new QVBoxLayout;
    lay->addWidget(life);
    lay->addWidget(score);
    Player1=new QSoundEffect;
    Player1->setSource(QUrl::fromLocalFile((FDSOUND)));
    setLayout(lay);
    win = false;
    winPlayed = false;
    initial();
}
Classic2::~Classic2()
{
    setAttribute(Qt::WA_DeleteOnClose,true);
}
//设置定时器间隔，每10ms刷新一次
void Classic2::initial()
{
    Timer.setInterval(8);
    fruitRecorded=0;
    startGame();
    srand((unsigned int)time(NULL));
}
void Classic2::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.drawPixmap(myKnife.x,myKnife.y,myKnife.myKnife);
    for(int i=0;i<20;i++)
    {
        if(!banana[i].isFree)
        {
            if(!banana[i].isDestroyed)
                painter.drawPixmap(banana[i].x,banana[i].y,banana[i].banana);
        }
    }
    for(int i=0;i<20;i++)
    {
        if(!watermelon[i].isFree)
        {
            if(!watermelon[i].isDestroyed)
                painter.drawPixmap(watermelon[i].x,watermelon[i].y,watermelon[i].watermelon);
        }
    }
    for(int i=0;i<20;i++)
    {
        if(!apple[i].isFree)
        {
            if(!apple[i].isDestroyed)
                painter.drawPixmap(apple[i].x,apple[i].y,apple[i].apple);
        }
    }
    for(int i=0;i<20;i++)
    {
        if(!peach[i].isFree)
        {
            if(!peach[i].isDestroyed)
                painter.drawPixmap(peach[i].x,peach[i].y,peach[i].peach);
        }
    }
    for(int i=0;i<20;i++)
    {
        if(!strawberry[i].isFree)
        {
            if(!strawberry[i].isDestroyed)
                painter.drawPixmap(strawberry[i].x,strawberry[i].y,strawberry[i].strawberry);
        }
    }
    for(int i=0;i<8;i++)
    {
        if(!bomb[i].isFree)
        {
            if(!bomb[i].isDestroyed)
                painter.drawPixmap(bomb[i].x,bomb[i].y,bomb[i].bomb);
            else if(!bomb[i].PRO.isPlayde)
                painter.drawPixmap(bomb[i].x,bomb[i].y,bomb[i].PRO.proPix[bomb[i].PRO.index]);
        }
    }
    painter.drawPixmap(myKnife.x,myKnife.y,myKnife.myKnife);
}
void Classic2::mouseMoveEvent(QMouseEvent *E){
    int x = E->x()-18;
    int y = E->y()-15;
    if(x>0&&x<575)
        myKnife.x = x;
    if(y>0&&y<505)
        myKnife.y = y;
    update();
}
void Classic2::updatePositino()
{
    life->setText(QString(LIFE).arg(myKnife.life));	//随时更新相关信息
    score->setText(QString(SCORE).arg(Score));
    for(int i=0;i<8;i++)
    {
        if(!bomb[i].isFree&&!bomb[i].isDestroyed){
            bomb[i].updatePosition();
        }
        if(bomb[i].isDestroyed&&!bomb[i].isFree){
            bomb[i].PRO.updateInfo();
        }
    }
    for(int i=0;i<20;i++)
    {
        if(!watermelon[i].isFree&&!watermelon[i].isDestroyed){
            watermelon[i].updatePosition();
        }
        if(watermelon[i].isDestroyed&&!watermelon[i].isFree){
            Player1->play();
            watermelon[i].isFree=true;
        }
    }
    for(int i=0;i<20;i++)
    {
        if(!apple[i].isFree&&!apple[i].isDestroyed){
            apple[i].updatePosition();
        }
        if(apple[i].isDestroyed&&!apple[i].isFree){
            Player1->play();
            apple[i].isFree=true;
        }
    }
    for(int i=0;i<20;i++)
    {
        if(!banana[i].isFree&&!banana[i].isDestroyed){
            banana[i].updatePosition();
        }
        if(banana[i].isDestroyed&&!banana[i].isFree){
            Player1->play();
            banana[i].isFree=true;
        }
    }
    for(int i=0;i<20;i++)
    {
        if(!peach[i].isFree&&!peach[i].isDestroyed){
            peach[i].updatePosition();
        }
        if(peach[i].isDestroyed&&!peach[i].isFree){
            Player1->play();
            peach[i].isFree=true;
        }
    }
    for(int i=0;i<20;i++)
    {
        if(!strawberry[i].isFree&&!strawberry[i].isDestroyed){
            strawberry[i].updatePosition();
        }
        if(strawberry[i].isDestroyed&&!strawberry[i].isFree){
            Player1->play();
            strawberry[i].isFree=true;
        }
    }
}
void Classic2::startGame(){

    Timer.start();
    connect(&Timer , &QTimer::timeout,[=](){
        FruitShow();
        updatePositino();
        collisionDetetion();
        update();
        endGame();
    });
}
void Classic2::FruitShow()
{
    fruitRecorded++;
    if(fruitRecorded<200)
        return;
    fruitRecorded=0;
    int fruitCount;
    fruitCount=rand()%4+5;
    int bombCount;
    bombCount=rand()%4;
    int appleCount;
    appleCount=rand()%4;
    int watermelonCount;
    watermelonCount=rand()%(fruitCount-bombCount-appleCount+1);
    int bananaCount;
    bananaCount=fruitCount-bombCount-watermelonCount;
    int peachCount;
    peachCount=fruitCount-bombCount-bananaCount;
    int strawberryCount;
    strawberryCount=fruitCount-bombCount-peachCount;
    for(int i=0,j=0;i<bombCount&&(i+j<20);j++){
        if(bomb[i+j].isFree){
            bomb[i+j].isReverse=rand()%2;
            bomb[i+j].isFree = false;
            bomb[i+j].x = bomb[i+j].isReverse*(600-bomb[i+j].bomb.width());
            bomb[i+j].y = rand()%300+175;
            i++;
        }
    }
    for(int i=0,j=0;i<appleCount&&(i+j<20);j++)
        if(apple[i+j].isFree){
            apple[i+j].isReverse=rand()%2;
            apple[i+j].isFree = false;
            apple[i+j].isMissed=false;
            apple[i+j].isDestroyed=false;
            apple[i+j].x = apple[i+j].isReverse*(600-apple[i+j].apple.width());
            apple[i+j].y = rand()%300+175;
            i++;
        }
    for(int i=0,j=0;i<watermelonCount&&(i+j<20);j++){
        if(watermelon[i+j].isFree){
            watermelon[i+j].isReverse=rand()%2;
            watermelon[i+j].isMissed=false;
            watermelon[i+j].isDestroyed=false;
            watermelon[i+j].isFree = false;
            watermelon[i+j].x = watermelon[i+j].isReverse*(600-watermelon[i+j].watermelon.width());
            watermelon[i+j].y = rand()%300+175;
            i++;
        }
    }
    for(int i=0,j=0;i<bananaCount&&(i+j<20);j++){
        if(banana[i+j].isFree){
            banana[i+j].isReverse=rand()%2;
            banana[i+j].isMissed=false;
            banana[i+j].isDestroyed=false;
            banana[i+j].isFree = false;
            banana[i+j].x = banana[i+j].isReverse*(600-banana[i+j].banana.width());
            banana[i+j].y = rand()%300+175;
            i++;
        }
    }
    for(int i=0,j=0;i<peachCount&&(i+j<20);j++){
        if(peach[i+j].isFree){
            peach[i+j].isReverse=rand()%2;
            peach[i+j].isMissed=false;
            peach[i+j].isDestroyed=false;
            peach[i+j].isFree = false;
            peach[i+j].x = peach[i+j].isReverse*(600-peach[i+j].peach.width());
            peach[i+j].y = rand()%300+175;
            i++;
        }
    }
    for(int i=0,j=0;i<strawberryCount&&(i+j<20);j++){
        if(strawberry[i+j].isFree){
            strawberry[i+j].isReverse=rand()%2;
            strawberry[i+j].isMissed=false;
            strawberry[i+j].isDestroyed=false;
            strawberry[i+j].isFree = false;
            strawberry[i+j].x = peach[i+j].isReverse*(600-peach[i+j].peach.width());
            strawberry[i+j].y = rand()%300+175;
            i++;
        }
    }
}

void Classic2::collisionDetetion()
{
    for(int i=0;i<20;i++)
    {
        if(!banana[i].isFree&&!banana[i].isDestroyed)
        {
            if(getDistanceBAK(banana[i], myKnife)<50)
            {
                Score++;
                banana[i].isDestroyed=true;
            }
        }
        else if(banana[i].isMissed)
        {
            myKnife.life--;
            banana[i].isMissed=false;
        }
    }

    for(int i=0;i<20;i++)
    {
        if(!watermelon[i].isFree&&!watermelon[i].isDestroyed)
        {
            if(getDistanceWAK(watermelon[i], myKnife)<50)
            {
                Score++;
                watermelon[i].isDestroyed=true;
            }
            else if(watermelon[i].isMissed)
            {
                myKnife.life--;
                watermelon[i].isMissed=false;
            }
        }
    }

    for(int i=0;i<20;i++)
    {
        if(!apple[i].isFree&&!apple[i].isDestroyed)
        {
            if(getDistanceAAK(apple[i], myKnife)<50)
            {
                Score++;
                apple[i].isDestroyed=true;
            }
            else if(apple[i].isMissed)
            {
                myKnife.life--;
                apple[i].isMissed=false;
            }
        }
    }
    for(int i=0;i<20;i++)
    {
        if(!peach[i].isFree&&!peach[i].isDestroyed)
        {
            if(getDistancePAK(peach[i], myKnife)<50)
            {
                Score++;
                peach[i].isDestroyed=true;
            }
            else if(peach[i].isMissed)
            {
                myKnife.life--;
                peach[i].isMissed=false;
            }
        }
    }
    for(int i=0;i<20;i++)
    {
        if(!strawberry[i].isFree&&!strawberry[i].isDestroyed)
        {
            if(getDistanceSAK(strawberry[i], myKnife)<50)
            {
                Score++;
                strawberry[i].isDestroyed=true;
            }
            else if(strawberry[i].isMissed)
            {
                myKnife.life--;
                strawberry[i].isMissed=false;
            }
        }
    }
    for(int i=0;i<8;i++)
    {
        if(!bomb[i].isFree&&!bomb[i].isDestroyed)
        {
            if(getDistanceTAK(bomb[i], myKnife)<30)
            {
                myKnife.life--;
                bomb[i].isDestroyed=true;
                bomb[i].PRO.x = bomb[i].x;
                bomb[i].PRO.y = bomb[i].y;
            }
        }
    }
}
void Classic2::endGame(){
    if(myKnife.life<=0&&!myKnife.isPlayed){
        myKnife.isPlayed = true;
        EndGame *e = new EndGame(Score);
        cout<<myKnife.life;
        cout<<myKnife.isPlayed;
        cout<<winPlayed;
        e->show();
        this->close();
    }
}
int Classic2::getDistanceBAK(Banana B,Knife K)
{
    return sqrt((B.x-20-K.x)*(B.x-20-K.x)+(B.y+20-K.y)*(B.y+20-K.y));
}

int Classic2::getDistanceWAK(Watermelon W,Knife K)
{
    return sqrt((W.x-20-K.x)*(W.x-20-K.x)+(W.y+20-K.y)*(W.y+20-K.y));
}

int Classic2::getDistanceTAK(Bomb T,Knife K)
{
    return sqrt((T.x-20-K.x)*(T.x-20-K.x)+(T.y+20-K.y)*(T.y+20-K.y));
}

int Classic2::getDistanceAAK(Apple A, Knife K)
{
    return sqrt((A.x-20-K.x)*(A.x-20-K.x)+(A.y+20-K.y)*(A.y+20-K.y));
}

int Classic2::getDistancePAK(Peach P, Knife K)
{
    return sqrt((P.x-20-K.x)*(P.x-20-K.x)+(P.y+20-K.y)*(P.y+20-K.y));
}
int Classic2::getDistanceSAK(Strawberry S, Knife K)
{
    return sqrt((S.x-20-K.x)*(S.x-20-K.x)+(S.y+20-K.y)*(S.y+20-K.y));
}
